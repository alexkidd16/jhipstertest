/* globals $ */
'use strict';

angular.module('testjhipsterApp')
    .directive('testjhipsterAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
