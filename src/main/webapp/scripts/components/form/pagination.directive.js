/* globals $ */
'use strict';

angular.module('testjhipsterApp')
    .directive('testjhipsterAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
