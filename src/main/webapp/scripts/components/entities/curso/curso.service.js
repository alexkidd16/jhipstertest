'use strict';

angular.module('testjhipsterApp')
    .factory('Curso', function ($resource, DateUtils) {
        return $resource('api/cursos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
