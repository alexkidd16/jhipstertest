'use strict';

angular.module('testjhipsterApp').controller('CursoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Curso',
        function($scope, $stateParams, $modalInstance, entity, Curso) {

        $scope.curso = entity;
        $scope.load = function(id) {
            Curso.get({id : id}, function(result) {
                $scope.curso = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('testjhipsterApp:cursoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.curso.id != null) {
                Curso.update($scope.curso, onSaveFinished);
            } else {
                Curso.save($scope.curso, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
