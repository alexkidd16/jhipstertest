'use strict';

angular.module('testjhipsterApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('curso', {
                parent: 'entity',
                url: '/cursos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'testjhipsterApp.curso.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/curso/cursos.html',
                        controller: 'CursoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('curso');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('curso.detail', {
                parent: 'entity',
                url: '/curso/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'testjhipsterApp.curso.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/curso/curso-detail.html',
                        controller: 'CursoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('curso');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Curso', function($stateParams, Curso) {
                        return Curso.get({id : $stateParams.id});
                    }]
                }
            })
            .state('curso.new', {
                parent: 'curso',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/curso/curso-dialog.html',
                        controller: 'CursoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nome: null, ativo: null, modalidade: null, cargaHoraria: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('curso', null, { reload: true });
                    }, function() {
                        $state.go('curso');
                    })
                }]
            })
            .state('curso.edit', {
                parent: 'curso',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/curso/curso-dialog.html',
                        controller: 'CursoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Curso', function(Curso) {
                                return Curso.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('curso', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
