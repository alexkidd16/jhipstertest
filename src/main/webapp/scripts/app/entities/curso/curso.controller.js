'use strict';

angular.module('testjhipsterApp')
    .controller('CursoController', function ($scope, Curso, ParseLinks) {
        $scope.cursos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Curso.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cursos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Curso.get({id: id}, function(result) {
                $scope.curso = result;
                $('#deleteCursoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Curso.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCursoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.curso = {nome: null, ativo: null, modalidade: null, cargaHoraria: null, id: null};
        };
    });
