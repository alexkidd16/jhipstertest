'use strict';

angular.module('testjhipsterApp')
    .controller('CursoDetailController', function ($scope, $rootScope, $stateParams, entity, Curso) {
        $scope.curso = entity;
        $scope.load = function (id) {
            Curso.get({id: id}, function(result) {
                $scope.curso = result;
            });
        };
        $rootScope.$on('testjhipsterApp:cursoUpdate', function(event, result) {
            $scope.curso = result;
        });
    });
