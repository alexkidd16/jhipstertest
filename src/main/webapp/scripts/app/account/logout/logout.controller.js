'use strict';

angular.module('testjhipsterApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
