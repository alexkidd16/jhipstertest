package br.com.miracleworks.testjh.repository;

import br.com.miracleworks.testjh.domain.Curso;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Curso entity.
 */
public interface CursoRepository extends JpaRepository<Curso,Long> {

}
