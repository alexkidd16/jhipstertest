package br.com.miracleworks.testjh.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.miracleworks.testjh.domain.Curso;
import br.com.miracleworks.testjh.repository.CursoRepository;
import br.com.miracleworks.testjh.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Curso.
 */
@RestController
@RequestMapping("/api")
public class CursoResource {

    private final Logger log = LoggerFactory.getLogger(CursoResource.class);

    @Inject
    private CursoRepository cursoRepository;

    /**
     * POST  /cursos -> Create a new curso.
     */
    @RequestMapping(value = "/cursos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Curso> create(@Valid @RequestBody Curso curso) throws URISyntaxException {
        log.debug("REST request to save Curso : {}", curso);
        if (curso.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new curso cannot already have an ID").body(null);
        }
        Curso result = cursoRepository.save(curso);
        return ResponseEntity.created(new URI("/api/cursos/" + curso.getId())).body(result);
    }

    /**
     * PUT  /cursos -> Updates an existing curso.
     */
    @RequestMapping(value = "/cursos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Curso> update(@Valid @RequestBody Curso curso) throws URISyntaxException {
        log.debug("REST request to update Curso : {}", curso);
        if (curso.getId() == null) {
            return create(curso);
        }
        Curso result = cursoRepository.save(curso);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /cursos -> get all the cursos.
     */
    @RequestMapping(value = "/cursos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Curso>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Curso> page = cursoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cursos", offset, limit);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /cursos/:id -> get the "id" curso.
     */
    @RequestMapping(value = "/cursos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Curso> get(@PathVariable Long id) {
        log.debug("REST request to get Curso : {}", id);
        return Optional.ofNullable(cursoRepository.findOne(id))
            .map(curso -> new ResponseEntity<>(
                curso,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cursos/:id -> delete the "id" curso.
     */
    @RequestMapping(value = "/cursos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Curso : {}", id);
        cursoRepository.delete(id);
    }
}
