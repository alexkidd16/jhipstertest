/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package br.com.miracleworks.testjh.web.rest.dto;
