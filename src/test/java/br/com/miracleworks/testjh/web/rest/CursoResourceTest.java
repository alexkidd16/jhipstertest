package br.com.miracleworks.testjh.web.rest;

import br.com.miracleworks.testjh.Application;
import br.com.miracleworks.testjh.domain.Curso;
import br.com.miracleworks.testjh.repository.CursoRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CursoResource REST controller.
 *
 * @see CursoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CursoResourceTest {

    private static final String DEFAULT_NOME = "SAMPLE_TEXT";
    private static final String UPDATED_NOME = "UPDATED_TEXT";

    private static final Integer DEFAULT_ATIVO = 0;
    private static final Integer UPDATED_ATIVO = 1;
    private static final String DEFAULT_MODALIDADE = "SAMPLE_TEXT";
    private static final String UPDATED_MODALIDADE = "UPDATED_TEXT";
    private static final String DEFAULT_CARGA_HORARIA = "SAMPLE_TEXT";
    private static final String UPDATED_CARGA_HORARIA = "UPDATED_TEXT";

    @Inject
    private CursoRepository cursoRepository;

    private MockMvc restCursoMockMvc;

    private Curso curso;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CursoResource cursoResource = new CursoResource();
        ReflectionTestUtils.setField(cursoResource, "cursoRepository", cursoRepository);
        this.restCursoMockMvc = MockMvcBuilders.standaloneSetup(cursoResource).build();
    }

    @Before
    public void initTest() {
        curso = new Curso();
        curso.setNome(DEFAULT_NOME);
        curso.setAtivo(DEFAULT_ATIVO);
        curso.setModalidade(DEFAULT_MODALIDADE);
        curso.setCargaHoraria(DEFAULT_CARGA_HORARIA);
    }

    @Test
    @Transactional
    public void createCurso() throws Exception {
        int databaseSizeBeforeCreate = cursoRepository.findAll().size();

        // Create the Curso
        restCursoMockMvc.perform(post("/api/cursos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(curso)))
                .andExpect(status().isCreated());

        // Validate the Curso in the database
        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeCreate + 1);
        Curso testCurso = cursos.get(cursos.size() - 1);
        assertThat(testCurso.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testCurso.getAtivo()).isEqualTo(DEFAULT_ATIVO);
        assertThat(testCurso.getModalidade()).isEqualTo(DEFAULT_MODALIDADE);
        assertThat(testCurso.getCargaHoraria()).isEqualTo(DEFAULT_CARGA_HORARIA);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cursoRepository.findAll().size();
        // set the field null
        curso.setNome(null);

        // Create the Curso, which fails.
        restCursoMockMvc.perform(post("/api/cursos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(curso)))
                .andExpect(status().isBadRequest());

        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModalidadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cursoRepository.findAll().size();
        // set the field null
        curso.setModalidade(null);

        // Create the Curso, which fails.
        restCursoMockMvc.perform(post("/api/cursos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(curso)))
                .andExpect(status().isBadRequest());

        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCargaHorariaIsRequired() throws Exception {
        int databaseSizeBeforeTest = cursoRepository.findAll().size();
        // set the field null
        curso.setCargaHoraria(null);

        // Create the Curso, which fails.
        restCursoMockMvc.perform(post("/api/cursos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(curso)))
                .andExpect(status().isBadRequest());

        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCursos() throws Exception {
        // Initialize the database
        cursoRepository.saveAndFlush(curso);

        // Get all the cursos
        restCursoMockMvc.perform(get("/api/cursos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(curso.getId().intValue())))
                .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
                .andExpect(jsonPath("$.[*].ativo").value(hasItem(DEFAULT_ATIVO)))
                .andExpect(jsonPath("$.[*].modalidade").value(hasItem(DEFAULT_MODALIDADE.toString())))
                .andExpect(jsonPath("$.[*].cargaHoraria").value(hasItem(DEFAULT_CARGA_HORARIA.toString())));
    }

    @Test
    @Transactional
    public void getCurso() throws Exception {
        // Initialize the database
        cursoRepository.saveAndFlush(curso);

        // Get the curso
        restCursoMockMvc.perform(get("/api/cursos/{id}", curso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(curso.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.ativo").value(DEFAULT_ATIVO))
            .andExpect(jsonPath("$.modalidade").value(DEFAULT_MODALIDADE.toString()))
            .andExpect(jsonPath("$.cargaHoraria").value(DEFAULT_CARGA_HORARIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCurso() throws Exception {
        // Get the curso
        restCursoMockMvc.perform(get("/api/cursos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCurso() throws Exception {
        // Initialize the database
        cursoRepository.saveAndFlush(curso);

		int databaseSizeBeforeUpdate = cursoRepository.findAll().size();

        // Update the curso
        curso.setNome(UPDATED_NOME);
        curso.setAtivo(UPDATED_ATIVO);
        curso.setModalidade(UPDATED_MODALIDADE);
        curso.setCargaHoraria(UPDATED_CARGA_HORARIA);
        restCursoMockMvc.perform(put("/api/cursos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(curso)))
                .andExpect(status().isOk());

        // Validate the Curso in the database
        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeUpdate);
        Curso testCurso = cursos.get(cursos.size() - 1);
        assertThat(testCurso.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testCurso.getAtivo()).isEqualTo(UPDATED_ATIVO);
        assertThat(testCurso.getModalidade()).isEqualTo(UPDATED_MODALIDADE);
        assertThat(testCurso.getCargaHoraria()).isEqualTo(UPDATED_CARGA_HORARIA);
    }

    @Test
    @Transactional
    public void deleteCurso() throws Exception {
        // Initialize the database
        cursoRepository.saveAndFlush(curso);

		int databaseSizeBeforeDelete = cursoRepository.findAll().size();

        // Get the curso
        restCursoMockMvc.perform(delete("/api/cursos/{id}", curso.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Curso> cursos = cursoRepository.findAll();
        assertThat(cursos).hasSize(databaseSizeBeforeDelete - 1);
    }
}
